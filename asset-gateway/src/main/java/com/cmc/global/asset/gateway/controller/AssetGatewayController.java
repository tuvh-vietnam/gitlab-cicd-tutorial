package com.cmc.global.asset.gateway.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.ReactiveCircuitBreakerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cmc.global.asset.gateway.application.AssetsServiceClient;
import com.cmc.global.asset.gateway.application.UsersServiceClient;
import com.cmc.global.asset.gateway.dto.AssetDetails;
import com.cmc.global.asset.gateway.dto.Assets;
import com.cmc.global.asset.gateway.dto.AssetsDto;
import com.cmc.global.asset.gateway.dto.Users;
import com.cmc.global.asset.gateway.dto.UsersDetail;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import lombok.RequiredArgsConstructor;
import reactor.core.publisher.Mono;

@RestController
@RequiredArgsConstructor
@RequestMapping("/")
@EnableCircuitBreaker
public class AssetGatewayController {

    @Autowired
    private AssetsServiceClient assetServiceClient;

    @Autowired
    private UsersServiceClient usersServiceClient;

    @SuppressWarnings("unused")
    @Autowired
    private ReactiveCircuitBreakerFactory<?, ?> cbFactory;

    @GetMapping(value = "api/gateway/users/{userId}")
    @HystrixCommand(fallbackMethod = "reliable2")
    public Mono<UsersDetail> getUsersDetail(final @PathVariable int userId) {
        return usersServiceClient.getUsersDetail(userId);
    }

    public Mono<UsersDetail> reliable2(int userId) {
        UsersDetail emptyDto = new UsersDetail();
        return Mono.justOrEmpty(emptyDto);
    }

    @GetMapping(value = "api/gateway/users")
    @HystrixCommand(fallbackMethod = "reliable3")
    public Mono<Users> getAllUsers() {
        return usersServiceClient.getAllUsers();
    }

    public Mono<Users> reliable3() {
        Users emptyDto = new Users();
        return Mono.justOrEmpty(emptyDto);
    }

    @GetMapping("/")
    public List<UsersDetail> userTest() {
        return new ArrayList<UsersDetail>();
    }

    @GetMapping(value = "api/gateway/assets/{assetId}")
    @HystrixCommand(fallbackMethod = "reliable1")
    public Mono<AssetDetails> getAssetDetailsById(final @PathVariable int assetId) {
        return assetServiceClient.getAssetDetailsById(assetId);
    }

    @GetMapping(value = "api/gateway/assets/user/{userId}")
    @HystrixCommand(fallbackMethod = "reliable")
    public Mono<Assets> getAssetDetailsListByUserId(final @PathVariable int userId) {
        return assetServiceClient.getAssetDetailsListByUserId(userId);
    }

    public Mono<AssetDetails> reliable1(int assetId) {
        AssetDetails emptyDto = new AssetDetails();
        return Mono.justOrEmpty(emptyDto);
    }

    public Mono<Assets> reliable(int userId) {
        AssetsDto emptyDto = new AssetsDto();
        emptyDto.setAssetCode("Asset Service error");
        Assets aset = new Assets();
        List<AssetsDto> items = new ArrayList<AssetsDto>();
        items.add(emptyDto);
        aset.setItems(items);
        return Mono.justOrEmpty(aset);
    }

    @GetMapping(value = "api/gateway/assets")
    @HystrixCommand(fallbackMethod = "reliable")
    public Mono<Assets> getAllAssetDetails() throws Exception {
        return assetServiceClient.getAllAssetDetails();
    }

    public Mono<Assets> reliable() {
        AssetsDto emptyDto = new AssetsDto();
        emptyDto.setAssetCode("Asset Service error");
        Assets aset = new Assets();
        List<AssetsDto> items = new ArrayList<AssetsDto>();
        items.add(emptyDto);
        aset.setItems(items);
        return Mono.justOrEmpty(aset);
    }

    public AssetGatewayController(AssetsServiceClient assetServiceClient, UsersServiceClient usersServiceClient) {
        super();
        this.assetServiceClient = assetServiceClient;
        this.usersServiceClient = usersServiceClient;
    }

    /*
     * @RequestMapping("error") public String handleError(HttpServletRequest
     * request) { Object status =
     * request.getAttribute(RequestDispatcher.ERROR_STATUS_CODE);
     * 
     * if (status != null) { Integer statusCode =
     * Integer.valueOf(status.toString());
     * 
     * if (statusCode == HttpStatus.NOT_FOUND.value()) { return "error-404"; } else
     * if (statusCode == HttpStatus.INTERNAL_SERVER_ERROR.value()) { return
     * "error-500"; } } return "error"; }
     */

}
