package com.cmc.global.asset.gateway.controller;

import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import reactor.core.publisher.Mono;

@Controller
@RequestMapping("/")
public class AssetErrorController implements ErrorController {

    public AssetErrorController() {
        super();
    }

    @GetMapping("error")
    public Mono<String> handleError() {
        // do something like logging
        return Mono.just("error");
    }

    @Override
    public String getErrorPath() {
        return null;
    }

}
